package main

import (
	"fmt"
	"test1/container/queue"
)

func main() {
	q := queue.Queue{1, 2, 3}
	q.Push(4)
	q.Push(5)

	fmt.Println(q)
	fmt.Println(q.Pop())
	fmt.Println(q)

	// 放不同类型的数据到slice里面
	q2 := queue.Queue{"123", 1, "str", true, 1.12}
	fmt.Println(q2)
}
