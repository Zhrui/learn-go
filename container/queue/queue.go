package queue

type Queue []interface{}

func (q *Queue) Push(v interface{}) {
	// todo 为啥这里需要加上星号呢, node那里不用
	*q = append(*q, v)
}

func (q *Queue) Pop() interface{} {
	head := (*q)[0]
	*q = (*q)[1:]

	return head
}

// PopInt 如果该queue规定只能存int，则需要强制转换
func (q *Queue) PopInt() int {
	head := (*q)[0]
	*q = (*q)[1:]

	return head.(int)
}

func (q *Queue) IsEmpty() bool {
	return len(*q) == 0
}
