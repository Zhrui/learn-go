package tree

import "fmt"

type Node struct {
	Value       int
	Left, Right *Node
}

func test(node Node) {
	if node.Left == nil {

	}
}

// CreateNode 工厂函数 类似构造函数
func CreateNode(value int) *Node {
	return &Node{Value: value}
}

func (node *Node) Print() {
	if node == nil {
		return
	}
	fmt.Print(node.Value, " ")
}

func (node *Node) SetValue(v int) {
	node.Value = v
}
