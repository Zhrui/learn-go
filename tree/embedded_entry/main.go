package main

import (
	"test1/tree"
)

type myTreeNode struct {
	*tree.Node // embedding内嵌
}

func (myNode *myTreeNode) postOrder() {
	if myNode == nil || myNode.Node == nil {
		return
	}
	myNode.Print()
	// 这里可以直接访问.Left，相当于把tree.Node结构里的东西平铺在myTreeNode上
	left := myTreeNode{myNode.Left}
	left.Traverse()
	right := myTreeNode{myNode.Right}
	right.Traverse()
}

func main() {
	node := myTreeNode{
		Node: &tree.Node{Value: 2, Left: &tree.Node{Value: 3}, Right: &tree.Node{Value: 5}},
	}
	node.postOrder()
}
