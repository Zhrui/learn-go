package main

import (
	"fmt"
	"test1/tree"
)

type myTreeNode struct {
	node *tree.Node
}

func (myNode *myTreeNode) postOrder() {
	if myNode == nil || myNode.node == nil {
		return
	}

	// omg 这是构造函数啊，不是类型转换。。。
	left := myTreeNode{myNode.node.Left}
	left.postOrder()
	right := myTreeNode{myNode.node.Right}
	right.postOrder()
	myNode.node.Print()

}

func main() {
	var root tree.Node
	fmt.Println(root) // 0 nil nil

	root2 := tree.Node{Value: 20, Left: &tree.Node{Value: 15}}
	root2.Right = &tree.Node{Value: 5}
	fmt.Println(root2)
	// 访问成员, 全部都是用".",不需要使用"->"
	fmt.Println(root2.Left.Value)

	fmt.Println("使用构造函数")
	root2.Right.Left = tree.CreateNode(2)
	fmt.Println(root2.Right)

	fmt.Println("内部函数")
	root2.Print()
	root2.SetValue(99)
	root2.Print()

	// 如果取的是地址一样可以用这些内部函数
	pRoot := &root2
	pRoot.Print()
	pRoot.SetValue(22)
	pRoot.Print()

	fmt.Println()
	root2.Traverse()

	fmt.Println()
	fmt.Println("用另一个struct来遍历: ")
	myTreeNode := myTreeNode{node: &root2}
	myTreeNode.postOrder()
}
