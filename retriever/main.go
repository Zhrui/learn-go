package main

import (
	"fmt"
	"test1/retriever/mock"
	"test1/retriever/real"
	"time"
)

type Retriever interface {
	Get(string) string
}

type Poster interface {
	Post(url string, form map[string]string) string
}

const url = "http://api.ea-calibration.com/threads"

func download(r Retriever) string {
	return r.Get(url)
}

func post(p Poster) string {
	return p.Post(url, map[string]string{
		"contents": "test123",
	})
}

type RetrieverPostInterface interface {
	Retriever
	Poster
}

func session(s RetrieverPostInterface) string {
	s.Post(url, map[string]string{"contents": "test post"})
	return s.Get(url)
}

func main() {
	var r Retriever = &mock.Retriever{Content: "test123"}
	fmt.Println(download(r))
	r = &real.Retriever{
		UserAgent: "Mozilla/5.0",
		Timeout:   time.Minute,
	}
	fmt.Println(download(r))

	// 断言，告诉编译器我们retriever的类型，就可以调用该类型下的其他方法。如果不是运行时会报错
	if realRetriever, ok := r.(*real.Retriever); ok {
		fmt.Println(realRetriever.Timeout) // 断言后就可以访问timeout了
	} else {
		fmt.Println("This retriever is not real retriever")
	}

	// try session
	s := mock.Retriever{Content: "session init"}
	fmt.Println(session(&s))
}
