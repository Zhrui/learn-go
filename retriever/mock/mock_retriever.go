package mock

type Retriever struct {
	Content string
}

func (r *Retriever) Post(_ string, form map[string]string) string {
	r.Content = form["contents"]
	return "ok"
}

func (r *Retriever) Get(_ string) string {
	return r.Content
}
