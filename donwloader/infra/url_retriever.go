package infra

import (
	"io/ioutil"
	"net/http"
)

type Retriever struct{}

func (r Retriever) Get(url string) string {
	resp, err := http.Get(url)
	if err != nil {
		panic(err)
	}

	// 程序跑完之后运行
	defer resp.Body.Close()

	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	return string(bytes)
}
