package main

import (
	"fmt"
	"test1/testing"
)

type Retriever interface {
	Get(string) string
}

func getRetriever() Retriever {
	//return infra.Retriever{}
	return testing.Retriever{}
}

func main() {
	var retriever Retriever = getRetriever()
	fmt.Println(retriever.Get("http://api.ea-calibration.com/threads"))
}
