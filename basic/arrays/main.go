package main

import "fmt"

func printArray(arr [5]int) {
	for _, v := range arr {
		fmt.Println(v)
	}
}

// 传入数组指针改变值
// 错误的方法
func changeArr(arr *[5]int) {
	arr[1] = 100
	arr[2] = 200
	arr[3] = 300
}

// 正确的方法
func changeArr2(arr []int) {
	arr[1] = 99
	arr[2] = 98
	arr[3] = 97
}

func main() {
	var arr1 [5]int
	arr2 := [3]int{1, 2, 3}
	arr3 := [...]int{2, 4, 6, 8, 10} // 赋了初值可以省略长度
	var grid [3][4]int

	fmt.Println(arr1, arr2, arr3)
	fmt.Println(grid)

	// 遍历
	for i := 0; i < len(arr2); i++ {
		fmt.Println("[", arr2[i], "]")
	}

	// 类似foreach
	for i, v := range arr3 {
		fmt.Println(i, v)
	}

	// 如果只想要i
	for i := range arr3 {
		fmt.Println(i)
	}

	printArray(arr1)
	changeArr(&arr1)
	fmt.Println(arr1)
	changeArr2(arr1[:])
	fmt.Println(arr1)
}
