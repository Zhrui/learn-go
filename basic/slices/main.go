package main

import "fmt"

func updateSlice(s []int) {
	s[0] = 100
}

func main() {
	// slice始终是一个array的view
	arr := [...]int{1, 2, 3, 4, 5, 6, 7}
	fmt.Println("arr[2:6] =", arr[2:6])
	fmt.Println("arr[:6] =", arr[:6])
	fmt.Println("arr[2:] =", arr[2:])
	fmt.Println("arr[:] =", arr[:])
	updateSlice(arr[:])
	fmt.Println(arr)
	// reSlice
	s := arr[1:6]
	s = s[1:2]
	// slice扩展 extending slice
	// 可以向后拓展不能向前拓展
	s2 := arr[1:2]
	s3 := s2[:6]
	fmt.Println(s2)
	fmt.Println(s2[:])
	fmt.Println(s3)
	fmt.Println(s2, "len:", len(s2), "cap:", cap(s2))

	fmt.Println()
	// append切片
	s = arr[:6]
	s[1] = 456
	s2 = append(s, 10)
	s2[2] = 789
	s3 = append(s2, 11)
	s4 := append(s3, 12)
	s4[1] = 123 // 由于此时s4观察的不再是arr了，所以改变值不会影响arr
	fmt.Println(s, s2, s3, s4)
	// 这时原来的arr只保存了10，后面的因为超过长度就没存了
	// s3, s4多出来的部分保存在内部创建的一个新的array上
	// s3, s4 no logger view arr, 观察的是系统内部创建的arr
	fmt.Println(arr)
}
