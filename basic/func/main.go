package main

import (
	"fmt"
	"math"
	"reflect"
	"runtime"
)

func eval(a, b int, op string) (int, error) {
	switch op {
	case "+":
		return a + b, nil
	case "-":
		return a - b, nil
	case "*":
		return a * b, nil
	case "/":
		q, _ := div(a, b)
		return q, nil
	default:
		return 0, fmt.Errorf("unsupported operation: %s", op)
	}
}

// 函数返回两个值
func div(a, b int) (q, r int) {
	// 或者这样写
	// q = a / b
	// r = a % b
	// return

	return a / b, a % b
}

func evalV2(op func(int, int) int, a, b int) int {
	// 获得函数名字
	p := reflect.ValueOf(op).Pointer()
	opName := runtime.FuncForPC(p).Name()
	fmt.Println(opName)
	return op(a, b)
}

func add(a, b int) int {
	return a + b
}

// 可变参数
func sum(numbers ...int) int {
	sum := 0
	for i := range numbers {
		sum += numbers[i]
	}

	return sum
}

func main() {
	i, err := eval(1, 2, "*")
	if err != nil {
		panic(err)
	}
	fmt.Println(i)
	q, r := div(13, 5)
	fmt.Println(q, r)
	fmt.Println(div(15, 4))
	fmt.Println(evalV2(add, 9, 8))
	// 匿名函数
	// 改变pow满足eval参数op
	fmt.Println(evalV2(
		func(a, b int) int {
			return int(math.Pow(float64(a), float64(b)))
		},
		2, 10),
	)
	fmt.Println(sum(1, 2, 3, 4, 5))
}
