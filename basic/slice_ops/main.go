package main

import "fmt"

func printSlice(s []int) {
	fmt.Println("cap:", cap(s), "len:", len(s))
	fmt.Println(s)
}
func main() {
	var s []int
	for i := 0; i < 33; i++ {
		printSlice(s)
		s = append(s, i) // 每次装不下就扩充*2
	}

	// 预设定slice大小
	s1 := []int{1, 2, 3, 4, 5, 6}
	printSlice(s1)
	s2 := make([]int, 16)
	s3 := make([]int, 10, 32)
	printSlice(s2)
	printSlice(s3)

	// copying slice
	fmt.Println("copying")
	copy(s2, s1)
	printSlice(s2)

	// deleting slice
	fmt.Println("deleting")
	fmt.Println("删掉第4个元素")
	s2 = append(s2[:3], s2[4:]...)
	printSlice(s2)
	fmt.Println("popping from front")
	front := s2[0]
	s2 = s2[1:]
	fmt.Println(front)
	printSlice(s2)
	fmt.Println("popping from back")
	tail := s2[len(s2)-1]
	s2 = s2[:len(s2)-1]
	fmt.Println(tail)
	printSlice(s2)
}
