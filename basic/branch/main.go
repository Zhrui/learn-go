package main

import (
	"fmt"
	"os"
)

func grade(score int) string {
	g := ""
	switch {
	case score < 60:
		g = "d"
	case score < 80:
		g = "c"
	case score < 90:
		g = "b"
	case score <= 100:
		g = "a"
	case score < 0 || score >= 100:
		panic(fmt.Sprintf("Wrong score: %d", score))
	}

	return g
}

func main() {
	const filename = "abc.txt"
	file, err := os.ReadFile(filename)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("%s \n", file)
	fmt.Println(
		// grade(101),
		grade(90),
		grade(88),
		grade(77),
	)
}
