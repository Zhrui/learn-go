package main

import "fmt"

func main() {
	// 无序的
	m := map[string]string{
		"name":    "zrl",
		"course":  "golang",
		"site":    "imooc",
		"quality": "good",
	}

	m2 := make(map[string]int) // empty map
	var m3 map[string]string   // nil

	fmt.Println(m)
	fmt.Println(m2)
	fmt.Println(m3)

	fmt.Println("traversing: ")
	for k, v := range m {
		fmt.Println(k, v)
	}

	fmt.Println("Getting values:")
	fmt.Println(m["course"])

	fmt.Println("判断key到底存不存在")
	if causeName, ok := m["cause"]; ok {
		fmt.Println(causeName)
	} else {
		fmt.Println("key does not exist")
	}
	fmt.Println("deleting value")
	delete(m, "name")
	fmt.Println(m)
}
