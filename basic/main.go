package main

import (
	"fmt"
	"math"
)

var aa = 123 // 在函数外定义必须要用var关键字
// bb := true // 这样在函数外面不行

// 这样定义只用写一次var
var (
	bb = true
	cc = 123
)

func variableZeroValue() {
	var a int
	var s string
	fmt.Printf("%d, %q \n", a, s)
	var c, d int = 123, 456
	fmt.Println(c, d)
}

func variableTypeDeduction() {
	var a, b, c, d = 1, 2, true, "fqzef"
	fmt.Println(a, b, c, d)
}

func variableInShort() {
	a, b, c, d := 1, 2, true, "zef"
	fmt.Println(a, b, c, d)
}

func triangle() {
	a, b := 3, 4
	// 类型转换: int转float64, float64转int
	c := int(math.Sqrt(float64(a*a + b*b)))
	fmt.Println(c)
}

// 常量
func consts() {
	const filename = "abc.txt"
	const a, b = 3, 4
}

func enums() {
	const (
		cpp = iota // 自增
		_
		php
		java
		golang
	)
	fmt.Println(cpp, php, java, golang)
}

func main() {
	variableZeroValue()
	fmt.Println(bb, cc)
	triangle()
	consts()
	enums()
}
