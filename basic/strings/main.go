package main

import (
	"fmt"
)

func main() {
	s := "good吼哇!"
	fmt.Println(len(s))
	// ch is a rune
	for i, ch := range s {
		fmt.Println(i, ch)
	}

	for i, ch := range []rune(s) {
		fmt.Printf("%d %c ", i, ch)
	}
}
