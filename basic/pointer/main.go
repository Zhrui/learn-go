package main

import "fmt"

//func selfAdd(a *int) {
//	++a
//}

func swap(a, b int) (int, int) {
	return b, a
}

func swapWithPointer(a, b *int) {
	*a, *b = *b, *a
}

func main() {
	a := 2
	var b *int
	b = &a
	*b = 4
	fmt.Println(a)
	fmt.Println(&a)
	fmt.Println(*b)
	fmt.Println(&b) // 指针b存放的地址
	fmt.Println(b)
	c, d := 4, 5
	swapWithPointer(&c, &d)
	fmt.Println(c, d)
	c, d = swap(c, d)
	fmt.Println(c, d)
}
